insert into todo(id, username, description, target_date, is_done)
values(10001, 'kirito','Learn JPA', sysdate(),false);

insert into todo(id, username, description, target_date, is_done)
values(10002, 'kirito','Learn Angular', sysdate(),false);

insert into todo(id, username, description, target_date, is_done)
values(10003, 'kirito','Learn Android', sysdate(),false);
