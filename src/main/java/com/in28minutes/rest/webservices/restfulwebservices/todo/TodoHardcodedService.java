package com.in28minutes.rest.webservices.restfulwebservices.todo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TodoHardcodedService {

    public Logger todoLogger = LoggerFactory.getLogger(TodoHardcodedService.class);
    private static List<Todo> todos = new ArrayList<Todo>();
    private static int idCounter = 0;
    static {
        todos.add(new Todo(++idCounter,"kirito","Learn coding",new Date(), false));
        todos.add(new Todo(++idCounter,"kirito","Learn coding1",new Date(), false));
        todos.add(new Todo(++idCounter,"kirito","Learn coding2",new Date(), true));

    }

    public List<Todo> findAll() {
        return todos;
    }

    public Todo deleteById(long id){
        Todo todo = findById(id);
        if(todo==null){
            return null;
        }
        if(todos.remove(todo)){
            return todo;
        }
        return null;
    }

    public Todo findById(long id) {
        for(Todo todo:todos){
            if(todo.getId()==id){
                return todo;
            }
        }
        return null;
    }

    public Todo save(Todo todo){
        if(todo.getId() < 0){
            todoLogger.info(String.valueOf(idCounter));
            todo.setId(++idCounter);
            todos.add(todo);
        }else{
            todoLogger.info("Inside else");
            deleteById(todo.getId());
            todos.add(todo);
        }
        return todo;
    }

    public Todo createTodo(Todo todo){
        todo.setId(++idCounter);
        todos.add(todo);
        return todo;
    }


}
