package com.in28minutes.rest.webservices.restfulwebservices.todo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class TodoJPAResource {

    @Autowired
    private TodoHardcodedService todoService;

    private Logger logger = LoggerFactory.getLogger(TodoJPAResource.class);

    @Autowired
    private todoJpaRepository todoJpaRepo;



    @PostMapping("/jpa/users/{username}/todos")
    public ResponseEntity<Void> createTodo(
            @PathVariable String username,
            @RequestBody Todo todo
    ){
        //Location
        //Get current resource url
        //append the id
        logger.info("Inside post method");
        //Todo createdTodo = todoService.createTodo(todo);
        todo.setUsername(username);
        Todo createdTodo = todoJpaRepo.save(todo);

        URI createdUri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(createdTodo.getId()).toUri();

        return ResponseEntity.created(createdUri).build();
    }


//    @PostMapping("/jpa/users/{username}/todos/save")
//    public ResponseEntity<Void> createTodo2(
//            @PathVariable String username,
//            @RequestBody Todo todo
//    ){
//        //Location
//        //Get current resource url
//        //append the id
//        logger.info("Inside post method");
//        //Todo createdTodo = todoService.createTodo(todo);
//        todo.setUsername(username);
//        Todo createdTodo = todoJpaRepo.save(todo);
//
//        URI createdUri = ServletUriComponentsBuilder.fromCurrentRequest()
//                .path("/{id}").buildAndExpand(createdTodo.getId()).toUri();
//
//        return ResponseEntity.created(createdUri).build();
//    }


    @GetMapping("/jpa/users/{username}/todos")
    public List<Todo> getAllTodos(@PathVariable  String username){
        logger.info("Inside GET method");

        return todoJpaRepo.findByUsername(username);
        //return todoService.findAll();
    }

    @GetMapping("/jpa/users/{username}/todos/{id}")
    public Todo getTodo(@PathVariable  String username,@PathVariable Long id){

        return todoJpaRepo.findById(id).get();
        //return todoService.findById(id);
    }


    //DELETE /users/{username}/todos/{id}
    @DeleteMapping("/jpa/users/{username}/todos/{id}")
    public ResponseEntity<Void> deleteTodo(@PathVariable String username,@PathVariable long id){
        //Todo todo = todoService.deleteById(id);
        todoJpaRepo.deleteById(id);
        return ResponseEntity.noContent().build();
    }


    @PutMapping("/jpa/users/{username}/todos/{id}")
    public ResponseEntity<Todo> updateTodo(
            @PathVariable String username,
            @PathVariable long id,
            @RequestBody Todo todo
    ){

        //Todo todoUpdated = todoService.save(todo);
        todo.setUsername(username);
        Todo todoUpdated = todoJpaRepo.save(todo);
        return new ResponseEntity<Todo>(todoUpdated, HttpStatus.OK);
    }


}
