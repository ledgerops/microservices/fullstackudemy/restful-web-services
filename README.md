### How to run the server?

```
mvn install
```

```
mvn spring-boot:start
```


### How to use the api's?


##### Get jwt token


Request

```
curl -i -X POST \
   -H "Origin:http://localhost:4200" \
   -H "Content-Type:application/json" \
   -d \
'{
  "username":"kirito",
  "password":"password"
  
}' \
 'http://localhost:8080/authenticate'

```

Response

```json
{
    "token": "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJraXJpdG8iLCJleHAiOjE1OTAwNDQ5MjIsImlhdCI6MTU4OTQ0MDEyMn0.D5jXME9vG2B4cZAm7GELOnLPUAa4RkcmRUxVmbWJm5cXk8Bl26GqceGoOPLvuS9HW05yE2VXKGXq5UEoGdGTkw"
}

```


##### Get list of todos

Request

```
curl -i -X GET \
   -H "Origin:http://localhost:4200" \
   -H "Authorization:Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJraXJpdG8iLCJleHAiOjE1OTAwNDQ5MjIsImlhdCI6MTU4OTQ0MDEyMn0.D5jXME9vG2B4cZAm7GELOnLPUAa4RkcmRUxVmbWJm5cXk8Bl26GqceGoOPLvuS9HW05yE2VXKGXq5UEoGdGTkw" \
 'http://localhost:8080/users/kirito/todos'
```


Response

```json
[
    {
        "id": 1,
        "username": "kirito",
        "description": "Learn coding",
        "targetDate": "2020-05-14T07:02:00.263+00:00",
        "done": false
    },
    {
        "id": 2,
        "username": "kirito",
        "description": "Learn coding1",
        "targetDate": "2020-05-14T07:02:00.263+00:00",
        "done": false
    },
    {
        "id": 3,
        "username": "kirito",
        "description": "Learn coding2",
        "targetDate": "2020-05-14T07:02:00.263+00:00",
        "done": true
    }
]


```

### How do i use jwt in my server?.

You will need jwt setup code and dependency to be present in your backend server. Basically jwt configuration is very time consuming and code is very long. So you can use this ready made utility to setup jwt. 

Copy jwt package and its contents as it is to your backend server. 

Copy dependency to pom.xml

```xml
		<dependency>
			<groupId>io.jsonwebtoken</groupId>
			<artifactId>jjwt</artifactId>
			<version>0.9.1</version>
		</dependency>
```

```
mvn install
```

```
mvn spring-boot:start
```


### How do i add new username and password to the backend?


- Go to ./src/main/java/com/in28minutes/rest/webservices/restfulwebservices/

- Open BcryptEncoderTest.java

Let say we want to add username -> asuna and password -> password

Generate encoded password using 

```java
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class BcryptEncoderTest {

   public static void main(String[] args){
       BCryptPasswordEncoder encode = new BCryptPasswordEncoder();

       System.out.println(encode.encode("password").toString());


   }
}
```

- Copy the encoded string 

- Go to JwtInMemoryUserDetailsService.java and add username and the encoded password.

```java
@Service
public class JwtInMemoryUserDetailsService implements UserDetailsService {

	static List<JwtUserDetails> inMemoryUserList = new ArrayList<>();

	static {
		inMemoryUserList.add(new JwtUserDetails(1L, "in28minutes",
				"$2a$10$3zHzb.Npv1hfZbLEU5qsdOju/tk2je6W6PnNnY.c1ujWPcZh4PL6e", "ROLE_USER_2"));
		inMemoryUserList.add(new JwtUserDetails(2L, "ranga",
				"$2a$10$IetbreuU5KihCkDB6/r1DOJO0VyU9lSiBcrMDT.biU7FOt2oqZDPm", "ROLE_USER_2"));
		inMemoryUserList.add(new JwtUserDetails(3L, "kirito",
				"$2a$10$Bs.HOGtXFEzGgNepUqvLj.V8xPimktH8C1fyL7AUziKAcvHxv2Cu6", "ROLE_USER_3"));
		
		//$2a$10$IetbreuU5KihCkDB6/r1DOJO0VyU9lSiBcrMDT.biU7FOt2oqZDPm
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<JwtUserDetails> findFirst = inMemoryUserList.stream()
				.filter(user -> user.getUsername().equals(username)).findFirst();

		if (!findFirst.isPresent()) {
			throw new UsernameNotFoundException(String.format("USER_NOT_FOUND '%s'.", username));
		}

		return findFirst.get();
	}

}
```

